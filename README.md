# Coxeter triangulations:

* [Pull request on github](https://github.com/GUDHI/gudhi-devel/pull/397)
* [Latest documentation](https://pages.saclay.inria.fr/vincent.rouvreau/gudhi/coxeter/group__coxeter__triangulation.html)

# Experimentations for the Ouragan team

## Equations

All the equations are summarized in [this document](equations_for_the_8_knot.pdf).

## The 8-knot

The aim of this experimentation was to build in a first step the [8-knot](knot/knot.mp4).

## Seifert surface

The aim of this experimentation was to build in a second step the [Seifert surface](seifert_surface/seifert_surface.mp4)
for a given angle `alpha` (where `c=cos(alpha)` and `s=sin(alpha)`).


## Fibration

The aim of this last experimentation was to build the [fibration](fibration/fibration.md).
