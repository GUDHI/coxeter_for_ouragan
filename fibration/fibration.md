# Fibration

## Resolution

One can modify Coxeter resolution by modifying:

```cpp
const double RESOLUTION = 0.1;
cox_tr.change_matrix(RESOLUTION * cox_tr.matrix());
```

## Euler characteristic

The idea was to increase the resolution until Euler characteristic to stabilize to 0.

I could not compute Euler characteristic from the Cell complex with a better resolution than 0.2 because of RAM issues.

We can compute Euler characteristic without the Cell complex to save some RAM.

| Resolution | Time           | Vertices | Euler char. | OFF file                                                                                                                |
| ---------- | -------------  | -------- | ----------- | ----------------------------------------------------------------------------------------------------------------------- |
| 1.         | 0.57 sec.      | 4682     | -46         | [1_fibration.off](https://pages.saclay.inria.fr/vincent.rouvreau/gudhi/coxeter_for_ouragan/1_fibration.off)    262 Ko |
| 0.5        | 11.8 sec.      | 57527    | -42         | [0.5_fibration.off](https://pages.saclay.inria.fr/vincent.rouvreau/gudhi/coxeter_for_ouragan/0.5_fibration.off)  3.2 Mo |
| 0.3        | 56.2 sec.      | 218095   | 4           | [0.3_fibration.off](https://pages.saclay.inria.fr/vincent.rouvreau/gudhi/coxeter_for_ouragan/0.3_fibration.off)   12 Mo |
| 0.2        | 4 mn.  8 sec.  | 676721   | -1          | [0.2_fibration.off](https://pages.saclay.inria.fr/vincent.rouvreau/gudhi/coxeter_for_ouragan/0.2_fibration.off)   32 Mo |
| 0.1        | 24 mn. 39 sec. | 5014294  | 0           | [0.1_fibration.off](https://pages.saclay.inria.fr/vincent.rouvreau/gudhi/coxeter_for_ouragan/0.1_fibration.off)  273 Mo |

## Betti numbers

Use the script [fibration_betti_numbers.py](fibration/fibration_betti_numbers.py)

As it is complicated to compute betti numbers from 5 millions of points in dimension 6, it can be computed directly from the simplicial complex.
