/*    Author(s): Vincent Rouvreau
 *
 *    Copyright (C) 2021 Inria - MIT license
 *
 *    Modification(s):
 */

#include <iostream>
#include <cmath>  // for std::sqrt
#include <unordered_map>

#include <gudhi/Coxeter_triangulation.h>
#include <gudhi/Implicit_manifold_intersection_oracle.h>  // for Gudhi::coxeter_triangulation::make_oracle
#include <gudhi/Manifold_tracing.h>
#include <gudhi/Cell_complex.h>

#include <gudhi/IO/build_mesh_from_cell_complex.h>
#include <gudhi/IO/output_meshes_to_medit.h>

#include <Eigen/Dense>

using namespace Gudhi::coxeter_triangulation;

struct Function_fibration {
  Eigen::VectorXd operator()(const Eigen::VectorXd& p) const {
    Eigen::VectorXd result = Eigen::VectorXd::Zero(k_);

    double x = p(0);
    double x_2 = x*x;
    double y = p(1);
    double y_2 = y*y;
    double z = p(2);
    double z_2 = z*z;
    double t = p(3);
    double t_2 = t*t;
    double c = p(4);
    double c_2 = c*c;
    double s = p(5);
    double s_2 = s*s;
    double rho = x_2 + y_2 + (z_2 - t_2) * (z_2 - t_2) + 4. * z_2 * t_2;

    result(0) = x_2 + y_2 + z_2 + t_2 - 1.;
    double f_1 = (z_2 - t_2) * rho + x * (8. * x_2 - 2. * rho);
    double f_2 = 2. * std::sqrt(2) * x * z * t + y * (8. * x_2 - rho);
    result(1) = c * f_1 + s * f_2;
    result(2) = c_2 + s_2 - 1.;
    return result;
  }

  /** \brief Returns the domain dimension. */
  std::size_t amb_d() const { return d_; };

  /** \brief Returns the codomain dimension. */
  std::size_t cod_d() const { return k_; };

  /** \brief Returns a point on the knot. */
  Eigen::VectorXd seed() const {
    Eigen::VectorXd result = Eigen::VectorXd::Zero(d_);
    result(2) = 1./std::sqrt(2.);
    result(3) = 1./std::sqrt(2.);
    result(4) = 1.;
    return result;
  }

  Function_fibration()
      : k_(3), d_(6) {}

 private:
  std::size_t k_, d_;
};

struct Fibration_boundary {
  Eigen::VectorXd operator()(const Eigen::VectorXd& p) const {
    Eigen::VectorXd result = Eigen::VectorXd::Zero(k_);

    double x = p(0);
    double x_2 = x*x;
    double y = p(1);
    double y_2 = y*y;
    double z = p(2);
    double z_2 = z*z;
    double t = p(3);
    double t_2 = t*t;
    double c = p(4);
    double s = p(5);
    double rho = x_2 + y_2 + (z_2 - t_2) * (z_2 - t_2) + 4. * z_2 * t_2;

    double f_1 = (z_2 - t_2) * rho + x * (8. * x_2 - 2. * rho);
    double f_2 = 2. * std::sqrt(2) * x * z * t + y * (8. * x_2 - rho);
    // s*f1-c*f2>0, in other words:
    result(0) = c * f_2 - s * f_1;
    return result;
  }

  /** \brief Returns the domain dimension. */
  std::size_t amb_d() const { return d_; };

  /** \brief Returns the codomain dimension. */
  std::size_t cod_d() const { return k_; };

  /** \brief Returns a point on the knot. */
  Eigen::VectorXd seed() const {
    Eigen::VectorXd result = Eigen::VectorXd::Zero(d_);
    result(2) = 1./std::sqrt(2.);
    result(3) = 1./std::sqrt(2.);
    result(4) = 1.;

    return result;
  }

  Fibration_boundary()
      : k_(4), d_(6) {}

 private:
  std::size_t k_, d_;
};

using Out_simplex_map = typename Manifold_tracing<Coxeter_triangulation<> >::Out_simplex_map;
using Simplex_handle = typename Manifold_tracing<Coxeter_triangulation<> >::Simplex_handle;
using Simplex_hash = typename Manifold_tracing<Coxeter_triangulation<> >::Simplex_hash;

int get_nb_n_simplex(const Out_simplex_map& smap, const std::size_t cod_d, const std::size_t level) {
  if (level == 0) {
    return smap.size();
  }
  std::unordered_set<Simplex_handle, Simplex_hash> n_simplex_set;
  for (auto& os_pair : smap) {
    const Simplex_handle& simplex = os_pair.first;
    for (Simplex_handle coface : simplex.coface_range(cod_d + level)) {
      n_simplex_set.insert(coface);
    }
  }
  return n_simplex_set.size();
}

int main(int argc, char** argv) {
  auto oracle = make_oracle(Function_fibration(),
                            Fibration_boundary()); 
  // Define a Coxeter triangulation.
  Coxeter_triangulation<> cox_tr(oracle.amb_d());
  // Theory forbids that a vertex of the triangulation lies exactly on the circle.
  // Add some offset to avoid algorithm degeneracies.
  cox_tr.change_offset(-Eigen::VectorXd::Random(oracle.amb_d()));
  // For a better manifold approximation, one can change the linear transformation matrix.
  // The number of points and edges will increase with a better resolution.
  cox_tr.change_matrix(0.05 * cox_tr.matrix());

  // Manifold tracing algorithm
  std::vector<Eigen::VectorXd> seed_points(1, oracle.seed());
  Out_simplex_map interior_simplex_map, boundary_simplex_map;
  //std::cout << "manifold_tracing_algorithm" << std::endl;
  manifold_tracing_algorithm(seed_points, cox_tr, oracle, interior_simplex_map, boundary_simplex_map);

  int nb_int_vrt = get_nb_n_simplex(interior_simplex_map, oracle.cod_d(), 0);
  int nb_bnd_vrt = get_nb_n_simplex(boundary_simplex_map, 4, 0);
  int nb_int_edg = get_nb_n_simplex(interior_simplex_map, oracle.cod_d(), 1);
  int nb_bnd_edg = get_nb_n_simplex(boundary_simplex_map, 4, 1);
  int nb_int_tri = get_nb_n_simplex(interior_simplex_map, oracle.cod_d(), 2);
  int nb_bnd_tri = get_nb_n_simplex(boundary_simplex_map, 4, 2);
  int nb_int_tet = get_nb_n_simplex(interior_simplex_map, oracle.cod_d(), 3);

  std::cout << "nOFF\n6 " << nb_int_vrt+nb_bnd_vrt << " 0 0\n";
  for (auto& pairs : interior_simplex_map) {
    std::cout << pairs.second(0) << " " << pairs.second(1) << " " << pairs.second(2) << " "
              << pairs.second(3) << " " << pairs.second(4) << " " << pairs.second(5) << std::endl;
  }
  for (auto& pairs : boundary_simplex_map) {
    std::cout << pairs.second(0) << " " << pairs.second(1) << " " << pairs.second(2) << " "
              << pairs.second(3) << " " << pairs.second(4) << " " << pairs.second(5) << std::endl;
  }

  std::cout << "## Vertices =  " << nb_int_vrt << "\t+ " << nb_bnd_vrt << "\t= " << nb_int_vrt+nb_bnd_vrt << std::endl;
  std::cout << "## Edges =     " << nb_int_edg << "\t+ " << nb_bnd_edg << "\t= " << nb_int_edg+nb_bnd_edg << std::endl;
  std::cout << "## Triangles = " << nb_int_tri << "\t+ " << nb_bnd_tri << "\t= " << nb_int_tri+nb_bnd_tri << std::endl;
  std::cout << "## Tetra =     " <<  nb_int_tet << std::endl;
  std::cout << "## Euler characteristic = " << nb_int_vrt + nb_bnd_vrt - nb_int_edg - nb_bnd_edg + nb_int_tri + nb_bnd_tri - nb_int_tet << std::endl;
  /*
  // Constructing the cell complex
  std::size_t intr_d = oracle.amb_d() - oracle.cod_d();
  Cell_complex<Out_simplex_map> cell_complex(intr_d);
  //std::cout << "construct_complex" << std::endl;
  cell_complex.construct_complex(interior_simplex_map, boundary_simplex_map);
  auto i_map = cell_complex.interior_simplex_cell_maps();
  auto b_map = cell_complex.boundary_simplex_cell_maps();
  */
  /*std::cout << "nOFF\n6 " << static_cast<int>(cell_complex.cell_point_map().size()) << " 0 0\n";
  for (auto pairs : cell_complex.cell_point_map()) {
    std::cout << pairs.second(0) << " " << pairs.second(1) << " " << pairs.second(2) << " "
              << pairs.second(3) << " " << pairs.second(4) << " " << pairs.second(5) << std::endl;
  }*/
  /*std::cout << "## From cell complex: Interior - Boundary" << std::endl;
  std::cout << "## Vertices: " << static_cast<int>(cell_complex.cell_point_map().size()) << std::endl;
  std::cout << "## Edges: "      << i_map[1].size() << " - " << b_map[1].size() << std::endl;
  std::cout << "## Triangles: "  << i_map[2].size() << " - " << b_map[2].size() << std::endl;
  std::cout << "## Tetrahedra: " << i_map[3].size() << " - " << b_map[3].size() << std::endl;

  std::cout << "## Euler: " << static_cast<int>(cell_complex.cell_point_map().size()) - (static_cast<int>(i_map[1].size()) + static_cast<int>(b_map[1].size())) 
                               + (static_cast<int>(i_map[2].size()) + static_cast<int>(b_map[2].size())) - static_cast<int>(i_map[3].size()) << std::endl;
  */
  //std::cout << "build_mesh_from_cell_complex" << std::endl;
  //auto mesh = build_mesh_from_cell_complex(cell_complex);

  /*std::cout << "nOFF\n6 " << mesh.vertex_points.size() << " 0 0\n";
  for (auto p : mesh.vertex_points) {
    std::cout << p(0) << " " << p(1) << " " << p(2) << " " << p(3) << " " << p(4) << " " << p(5) << " " << std::endl;
  }*/

  /*std::cout << "From mesh" << std::endl;
  std::cout << "# Vertices: " << mesh.vertex_points.size() << std::endl;
  std::cout << "# Edges: " << mesh.edges.size() << std::endl;
  std::cout << "# Triangles: " << mesh.triangles.size() << std::endl;
  std::cout << "# Tetrahedra: " << mesh.tetrahedra.size() << std::endl;
  std::cout << "# Euler characteristic: "
            << static_cast<int>(mesh.vertex_points.size()) - static_cast<int>(mesh.edges.size()) + 
               static_cast<int>(mesh.triangles.size())     - static_cast<int>(mesh.tetrahedra.size()) << std::endl;*/
  return 0;
}
