import gudhi as gd
import matplotlib.pyplot as plt
import time

pt = gd.subsampling.sparsify_point_set(off_file='0.2_fibration.off', min_squared_dist=0.05)
len(pt) # 5888
rips = gd.RipsComplex(points=pt, max_edge_length=1.2)
stree = rips.create_simplex_tree(max_dimension=1)
print(stree.num_simplices()) # 1476323

while (stree.num_simplices() > 100000):
    start_time = time.time()
    stree.collapse_edges()
    print(stree.num_simplices(), " simplices - ", (time.time() - start_time), " sec.")

# 662610  simplices -  676.4287972450256  sec.
# 534247  simplices -  82.2976131439209  sec.
# 433646  simplices -  43.11255645751953  sec.
# 354876  simplices -  25.746047735214233  sec.
# 295900  simplices -  15.94880485534668  sec.
# 251028  simplices -  10.434119701385498  sec.
# 217624  simplices -  6.936373233795166  sec.
# 191722  simplices -  4.863378286361694  sec.
# 171909  simplices -  3.4740004539489746  sec.
# 156737  simplices -  2.567256450653076  sec.
# 144551  simplices -  1.989370346069336  sec.
# 134895  simplices -  1.5648293495178223  sec.
# 127332  simplices -  1.265146255493164  sec.
# 121156  simplices -  1.0952098369598389  sec.
# 116226  simplices -  0.969074010848999  sec.
# 112159  simplices -  0.8279423713684082  sec.
# 108997  simplices -  0.7501764297485352  sec.
# 106388  simplices -  0.6864266395568848  sec.
# 104151  simplices -  0.6453402042388916  sec.
# 102261  simplices -  0.6065428256988525  sec.
# 100648  simplices -  0.5532405376434326  sec.
# 99334  simplices -  0.5942223072052002  sec.

stree.expansion(4)
print(stree.num_simplices()) # 4733680
# 8354797 for stree.expansion(5)
diag = stree.persistence()
stree.betti_numbers()
# [1, 1, 1, 3]
gd.plot_persistence_diagram(diag, legend=True)
plt.show()
